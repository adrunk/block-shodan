# README #

* block shodan.io ips via iptables

### What is shodan.io ###

* Shodan is a search engine which does not index web sites or web contents, but vulnerable devices on the internet.
* https://wiki.ipfire.org/configuration/firewall/blockshodan

### How do I get set up? ###

1. download shodan.ips and block-shodan.sh to /root/iptables or edit the .sh to your needs
2. chmod +x and run ./block-shodan.sh
3. to save these rules after reboot install iptables-persistent:
4. sudo apt install iptables-persistent
