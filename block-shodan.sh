#!/bin/bash
IPT=iptables
SPAMLIST="shodanlist"
SPAMDROPMSG="Shodan.io is not good"
BADIPS=$(egrep -v -E "^#|^$" /root/iptables/block-shodan/shodan.ips)
 
$IPT -N $SPAMLIST
 
for ipblock in $BADIPS
do
  $IPT -A $SPAMLIST -s $ipblock -j LOG --log-prefix "$SPAMDROPMSG"
  $IPT -A $SPAMLIST -s $ipblock -j DROP
done
 
$IPT -I INPUT -j $SPAMLIST
$IPT -I OUTPUT -j $SPAMLIST
$IPT -I FORWARD -j $SPAMLIST
iptables-save